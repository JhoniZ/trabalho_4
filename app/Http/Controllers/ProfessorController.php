<?php

namespace App\Http\Controllers;

use App\Professore;
use Illuminate\Http\Request;

class ProfessorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Professore::paginate(4);
        return Professore::with('ProfessorFunction')->paginate(4);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{

            $this->validate(
            $request,[
                'nomeprofessor' => 'required|min:2|max:50',
                'cpf' =>'required|min:11|max:11',
                'telefone' =>'required|min:9|max:30',
                'salario' => 'required|numeric'
            ]
        );

        $professor = new Professore();
        $professor->fill($request->all());
        $professor->save();

        if ($professor) {
            return response()->json([$professor], 201);
        }else
            return response()->json(["mensagem" => "Erro ao cadastrar Professor"], 400);
        }
        catch( \Exception $e){
            return "Ocorrou erro ao gravar professor";
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Professor  $professor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $professor = Professore::find($id);
        $professor = Professore::with('ProfessorFunction')->find($id);

        return $professor;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Professor  $professor
     * @return \Illuminate\Http\Response
     */
    public function edit(Professor $professor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Professor  $professor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $professor = Professore::find($id);

        $professor->fill($request->all());

        $professor->save();

        return "Alterou com sucesso";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Professor  $professor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $professor = Professore::find($id);

        $professor->delete();

        return "Deletou com sucesso";
    }
}
