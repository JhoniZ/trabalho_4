<?php

namespace App\Http\Controllers;

use App\Materia;
use Illuminate\Http\Request;
use Validator;
use Mockery\Exception;

class MateriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Materia::paginate(4);
        return Materia::with('MateriaFuncao')->paginate(4);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        try{
            $this->validate(
            $request,[
                'descricao' => 'required|max:200',
                'cod_professor' => 'required|numeric',
                'valorReoferta' => 'required|numeric',
                'qtdCursosAbrangidos' => 'required|numeric'
            ]
         );

        $materia = new Materia();
        $materia->fill($request->all());
        $materia->save();

         if( $materia ){
                return response()->json( [$materia], 201 );
            }else{
                return response()->json( ["mensagem" => "Erro ao cadastrar materiaj"], 400 );
            }
            
        }catch( \Exception $e ){
            return "Erro ao Guardar registro de Materia";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $materia = Materia::with('MateriaFuncao')->find($id);

        return $materia;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function edit(Materia $materia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $materia = Materia::find($id);

        $materia->fill($request->all());

        $materia->save();

        return "Alterou com sucesso";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Materia  $materia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $materia = Materia::find($id);
        $materia->delete();

       return "Deletou com sucesso";
    }
}
