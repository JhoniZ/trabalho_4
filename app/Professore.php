<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professore extends Model
{
    protected $fillable = ['nomeprofessor','cpf','salario','telefone'];

    public function ProfessorFunction()
    {
    	return $this->hasmany('App\Materia' ,'cod_professor', 'id');
    }
}
