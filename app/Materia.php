<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    protected $fillable = ['descricao', 'cod_professor', 'valorReoferta', 'qtdCursosAbrangidos'];

    public function MateriaFuncao()
    {
    	return $this->belongsto('App\Professore', 'cod_professor', 'id');
    }

}
