<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Materias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materias', function(Blueprint $table){
            $table->increments('id');
            $table->string('descricao',200);
            $table->integer('cod_professor')->unsigned();
            $table->double('valorReoferta');
            $table->integer('qtdCursosAbrangidos');
            $table->timestamps();
            $table->foreign('cod_professor')->references('id')->on('professores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('materias');
    }
}
