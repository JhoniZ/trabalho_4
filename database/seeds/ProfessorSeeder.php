<?php

use Illuminate\Database\Seeder;
use App\Professore;

class ProfessorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Professore::create([
        	'nomeprofessor' => 'Jose',
            'cpf' => '08632849905', 
            'salario' => 2.500,
            'telefone' => 999457682
        ]);


        Professore::create([
        	'nomeprofessor' => 'Claudio',
            'cpf' => '08632849905', 
            'salario' => 2.500,
            'telefone' => 999457682
        ]);

        Professore::create([
        	'nomeprofessor' => 'Marcos',
            'cpf' => '08632849905', 
            'salario' => 2.500,
            'telefone' => 999457682
        ]);

        Professore::create([
        	'nomeprofessor' => 'Felipe',
            'cpf' => '08632849905', 
            'salario' => 2.500,
            'telefone' => 999457682
        ]);

        Professore::create([
        	'nomeprofessor' => 'Antonio',
            'cpf' => '08632849905', 
            'salario' => 2.500,
            'telefone' => 999457682
        ]);
    }
}
