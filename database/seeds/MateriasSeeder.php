<?php

use Illuminate\Database\Seeder;
use App\Materia;

class MateriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Materia::create([
        	'descricao' => 'Matematica',
        	'cod_professor' => 1,
        	'valorReoferta' => 1000,
        	'qtdCursosAbrangidos' => 3
        ]);

		Materia::create([
        	'descricao' => 'TGS',
        	'cod_professor' => 2,
        	'valorReoferta' => 1000,
        	'qtdCursosAbrangidos' => 3
        ]);

		Materia::create([
        	'descricao' => 'SO',
        	'cod_professor' => 3,
        	'valorReoferta' => 1000,
        	'qtdCursosAbrangidos' => 3
        ]);

		Materia::create([
        	'descricao' => 'Programacao',
        	'cod_professor' => 4,
        	'valorReoferta' => 1000,
        	'qtdCursosAbrangidos' => 3
        ]);

		Materia::create([
        	'descricao' => 'Banco de dados',
        	'cod_professor' => 5,
        	'valorReoferta' => 1000,
        	'qtdCursosAbrangidos' => 3
        ]);

    }
}
